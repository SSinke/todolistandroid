package rs.ssoft.sinisa.todolistandroid.model;

/**
 * Created by sinisasladic on 10/19/16.
 */

public class Task {
    private long id;
    private String title;
    private String description;
    private boolean done = false;

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public String toString() {
        return title + " " + description;
    }
}