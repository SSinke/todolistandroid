package rs.ssoft.sinisa.todolistandroid.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import rs.ssoft.sinisa.todolistandroid.R;
import rs.ssoft.sinisa.todolistandroid.dao.TaskDAO;
import rs.ssoft.sinisa.todolistandroid.model.Task;

public class TaskDetails extends AppCompatActivity {
    private TaskDAO taskDAO;
    private Task oneTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);

        getCurrTask();

        initEditAct();

        deleteAct();
    }

    /**
     * Init db and get selected task
     */
    private void getCurrTask() {
        Intent intent = getIntent();
        long taskID = intent.getLongExtra("taskID",0);
        taskDAO = new TaskDAO(this);
        taskDAO.open();
        oneTask = taskDAO.getTasks(taskID);
    }

    /**
     * Init field and update action
     */
    private void initEditAct() {
        final EditText oneTaskDescription = (EditText) findViewById(R.id.oneTaskDescription);
        final EditText oneTaskTitle = (EditText) findViewById(R.id.oneTaskTitle);
        final CheckBox isDone = (CheckBox) findViewById(R.id.oneTaskIsDone);

        oneTaskDescription.setText(oneTask.getDescription());
        oneTaskTitle.setText(oneTask.getTitle());
        isDone.setChecked(oneTask.isDone());

        Button update = (Button) findViewById(R.id.oneTaskUpdate);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oneTask.setDescription(oneTaskDescription.getText().toString());
                oneTask.setTitle(oneTaskTitle.getText().toString());
                oneTask.setDone(isDone.isChecked());
                taskDAO.updateTask(oneTask);
                Intent intent1 = new Intent(TaskDetails.this,MainActivity.class);
                setResult(RESULT_OK,intent1);
                finish();
            }
        });
    }

    /**
     * Action on delete
     */
    private void deleteAct() {
        Button delete = (Button) findViewById(R.id.oneTaskDelete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taskDAO.deleteTask(oneTask);
                Intent intent1 = new Intent(TaskDetails.this,MainActivity.class);
                startActivity(intent1);
            }
        });
    }
}
