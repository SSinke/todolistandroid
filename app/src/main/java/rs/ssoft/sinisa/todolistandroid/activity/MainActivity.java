package rs.ssoft.sinisa.todolistandroid.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import rs.ssoft.sinisa.todolistandroid.R;
import rs.ssoft.sinisa.todolistandroid.adapter.TaskArrayAdapter;
import rs.ssoft.sinisa.todolistandroid.dao.TaskDAO;
import rs.ssoft.sinisa.todolistandroid.model.Task;

public class MainActivity extends AppCompatActivity {

    private TaskDAO datasource;
    private ArrayList<Task> tasksList;
    private TaskArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initDB();
        loadTask();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 2){
            if(resultCode == RESULT_OK){
                adapter.clear();
                tasksList = datasource.getAllTasks();
                adapter.addAll(tasksList);
            }
        }
    }

    /**
     * Create dialog for add new task
     */
    private void createDialog() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setTitle("Add task");
        dialog.setContentView(R.layout.add_task);

        Button addTaskBtn = (Button) dialog.findViewById(R.id.addtask);
        addTaskBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText description = (EditText) dialog.findViewById(R.id.taskDescription);
                EditText title = (EditText) dialog.findViewById(R.id.taskTitle);

                addNewTask(title.getText().toString(), description.getText().toString());

                dialog.cancel();
            }
        });

        dialog.show();
    }

    /**
     * Add new task
     *
     * @param title       Title of new task
     * @param description Description of new task
     */
    private void addNewTask(String title, String description) {
        tasksList.add(datasource.createTask(title, description));
        adapter.notifyDataSetChanged();
    }

    /**
     * Init database
     */
    private void initDB() {
        datasource = new TaskDAO(this);
        datasource.open();
    }

    /**
     * Load tasks in list view
     */
    private void loadTask() {
        tasksList = datasource.getAllTasks();

        adapter = new TaskArrayAdapter(this, tasksList);
        ListView listView = (ListView) findViewById(R.id.mobile_list);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sort_name_asc) {
            sortByTitleASC();
            return true;
        }else if (id == R.id.action_sort_name_desc) {
            sortByTitleDESC();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Sort task list by title ASC
     */
    private void sortByTitleASC() {
        Collections.sort(tasksList, new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getTitle().toLowerCase().compareTo(o2.getTitle().toLowerCase());
            }
        });

        adapter.notifyDataSetChanged();
    }


    /**
     * Sort task list by title ASC
     */
    private void sortByTitleDESC() {
        Collections.sort(tasksList, new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o2.getTitle().toLowerCase().compareTo(o1.getTitle().toLowerCase());
            }
        });

        adapter.notifyDataSetChanged();
    }
}
