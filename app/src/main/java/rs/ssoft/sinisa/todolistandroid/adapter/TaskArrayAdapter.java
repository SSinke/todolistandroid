package rs.ssoft.sinisa.todolistandroid.adapter;

/**
 * Created by sinisasladic on 10/18/16.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ssoft.sinisa.todolistandroid.R;
import rs.ssoft.sinisa.todolistandroid.activity.TaskDetails;
import rs.ssoft.sinisa.todolistandroid.activity.MainActivity;
import rs.ssoft.sinisa.todolistandroid.model.Task;

public class TaskArrayAdapter extends ArrayAdapter<Task> {
    private final Context context;
    private final ArrayList<Task> tasks;

    public TaskArrayAdapter(Context context, ArrayList<Task> tasks) {
        super(context, R.layout.rowlayout_task, tasks);
        this.context = context;
        this.tasks = tasks;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rowlayout_task, parent, false);
        final Task currTask = tasks.get(position);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity ma = (MainActivity) context;
                Intent intent = new Intent(context, TaskDetails.class);
                intent.putExtra("taskID", currTask.getId());
                ((MainActivity) context).startActivityForResult(intent, 2);
            }
        });

        TextView textView = (TextView) rowView.findViewById(R.id.label);
        TextView descriptionText = (TextView) rowView.findViewById(R.id.description);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        View doneView = (View) rowView.findViewById(R.id.task_done);


        textView.setText(currTask.getTitle());
        descriptionText.setText(currTask.getDescription());

        //imageView.setImageResource(R.drawable.ic_remove_circle_black_24dp);

        doneView.setVisibility(currTask.isDone() ? View.VISIBLE : View.INVISIBLE);

        return rowView;
    }
}