package rs.ssoft.sinisa.todolistandroid.dao;

/**
 * Created by sinisasladic on 10/19/16.
 */

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import rs.ssoft.sinisa.todolistandroid.model.Task;
import rs.ssoft.sinisa.todolistandroid.sql.MySQLiteHelper;

public class TaskDAO {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = {MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_DESCRIPTION, MySQLiteHelper.COLUMN_TITLE, MySQLiteHelper.COLUMN_DONE};

    public TaskDAO(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Task createTask(String title, String description) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_DESCRIPTION, description);
        values.put(MySQLiteHelper.COLUMN_TITLE, title);
        long insertId = database.insert(MySQLiteHelper.TABLE_TASKS, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_TASKS,
                allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Task newTask = cursorToTask(cursor);
        cursor.close();
        return newTask;
    }

    public void deleteTask(Task task) {
        long id = task.getId();
        System.out.println("Task " + id + "deleted!");
        database.delete(MySQLiteHelper.TABLE_TASKS, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }


    public void updateTask(Task task) {
        long id = task.getId();
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_DESCRIPTION, task.getDescription());
        values.put(MySQLiteHelper.COLUMN_TITLE, task.getTitle());
        values.put(MySQLiteHelper.COLUMN_DONE, task.isDone());

        database.update(MySQLiteHelper.TABLE_TASKS,values, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);
        System.out.println("Task " + id + "updated!");
    }

    public ArrayList<Task> getAllTasks() {
        ArrayList<Task> tasks = new ArrayList<>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_TASKS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Task task = cursorToTask(cursor);
            tasks.add(task);
            cursor.moveToNext();
        }

        cursor.close();
        return tasks;
    }

    public Task getTasks(long id) {

        Cursor cursor = database.query(MySQLiteHelper.TABLE_TASKS,
                allColumns, "_id = " + id , null, null, null, null);

        cursor.moveToFirst();
            Task task = cursorToTask(cursor);

        cursor.close();
        return task;
    }

    private Task cursorToTask(Cursor cursor) {
        Task task = new Task();
        task.setId(cursor.getLong(0));
        task.setDescription(cursor.getString(1));
        task.setTitle(cursor.getString(2));
        task.setDone(cursor.getString(3).equals("1"));
        return task;
    }
}
